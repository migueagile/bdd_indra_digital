#Author: Henry Andres Correa Correa
#Keywords Summary : Busqueda, Google
#Convensiones:
#Genero: 			1.Masculino 	2.Femenino	3.Sin Informacion
#direccionNo  V.Checked			F.Unchecked
@Google
Feature: Busqueda basica google
Story: Como community manager, quiero buscar en Google Tigo, para poder ver si la marca está bien posicionada.

  @BusquedaBasica
  Scenario: simple busqueda en Google
    Given un navegador web se encuentra en la página de Google
    When la frase de búsqueda "tigo" se introduce
    Then Resultados para "tigo" se muestran
