$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/features/SPIKE001-Aprendiendo.feature");
formatter.feature({
  "name": "paga tu factura",
  "description": "  mas informacion de paga tu factura",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Turorial"
    }
  ]
});
formatter.scenario({
  "name": "Verificacion de valor a pagar",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Turorial"
    },
    {
      "name": "@Tigo"
    }
  ]
});
formatter.step({
  "name": "Estamos en el sitio de PagaTuFactura",
  "keyword": "Given "
});
formatter.match({
  "location": "DefPagaTuFactura.estamos_en_el_sitio_de_PagaTuFactura()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Ingresamos el msisdn \"3043302450\"",
  "keyword": "When "
});
formatter.match({
  "location": "DefPagaTuFactura.ingresamos_el_msisdn(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "presionamos el boton confirmar",
  "keyword": "And "
});
formatter.match({
  "location": "DefPagaTuFactura.presionamos_el_boton_confirmar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "en el formulario ingresamos \"pruebasTigo@yopmail.com\"",
  "keyword": "And "
});
formatter.match({
  "location": "DefPagaTuFactura.en_el_formulario_ingresamos(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "guardo el valor a pagar",
  "keyword": "And "
});
formatter.match({
  "location": "DefPagaTuFactura.guardo_el_valor_a_pagar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "presiono el boton pagar factura",
  "keyword": "And "
});
formatter.match({
  "location": "DefPagaTuFactura.presiono_el_boton_pagar_factura()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "verifico el el valor a pagar coincida",
  "keyword": "Then "
});
formatter.match({
  "location": "DefPagaTuFactura.verifico_el_el_valor_a_pagar_coincida()"
});
formatter.result({
  "status": "passed"
});
});